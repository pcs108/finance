import pytest
from click.testing import CliRunner
from gpg_git_store import Store

from finance import Budget
from finance.__main__ import cli


@pytest.fixture(autouse=True)
def setup(created_store_path):
    store = Store.from_directory(created_store_path, item_class=Budget)

    budget1 = store.create_budget(id=1, name="foo")
    budget1.credits.create_item(id=1, name="abc", quantity=1, time=1)

    budget2 = store.create_budget(id=3, name="bar")
    budget2.credits.create_item(id=1, name="abc", quantity=5, time=2)

    budget3 = store.create_budget(id=5, name="boo")
    budget3.credits.create_item(id=1, name="abc", quantity=100, time=3)


@pytest.fixture
def runner():
    return CliRunner()


def test_cli_add_to_specified_budget_credits(runner):
    result = runner.invoke(cli, ["add", "1", "-q", "100"])
    assert result.exit_code == 0

    assert result.output == "  [1] foo 101\n"


def test_cli_create_adds_budget_to_store(runner):
    result = runner.invoke(cli, ["create", "hello"])
    assert result.exit_code == 0

    assert result.output == "  [6] hello 0\n"


def test_cli_create_sets_balance_when_specified(runner):
    result = runner.invoke(cli, ["create", "hello", "-b", "108"])
    assert result.exit_code == 0

    assert result.output == "  [6] hello 108\n"


def test_cli_ls_returns_alphabetic_list_of_budgets(runner):
    result = runner.invoke(cli, ["ls"])
    assert result.exit_code == 0
    expected = "  [3] bar 5\n  [5] boo 100\n  [1] foo 1\n"
    assert result.output == expected


def test_cli_mv_renames_specified_budget(runner):
    result = runner.invoke(cli, ["mv", "5", "noo"])
    assert result.exit_code == 0

    assert result.output == "  [5] noo 100\n"


def test_cli_rm_removes_budget_when_confirmed(runner):
    result = runner.invoke(cli, ["rm", "3"], input="y\n")
    assert result.exit_code == 0

    assert result.output == "Delete the budget? [y/N]: y\nDeleting: bar.\n"


def test_cli_rm_aborts_when_not_confirmed(runner):
    result = runner.invoke(cli, ["rm", "3"], input="n\n")
    assert result.exit_code == 1

    assert result.output == "Delete the budget? [y/N]: n\nAborted!\n"


def test_cli_rm_skips_confirmation_when_flagged(runner):
    result = runner.invoke(cli, ["rm", "3", "--force"])
    assert result.exit_code == 0


def test_cli_spend_deducts_from_specified_budget_balance(runner):
    result = runner.invoke(cli, ["spend", "5", "-q", "99"])
    assert result.exit_code == 0

    assert result.output == "  [5] boo 1\n"
