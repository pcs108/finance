from pathlib import Path

TESTS_ROOT = Path(__file__).parent
TEST_DATA = TESTS_ROOT / ".data"

STORE_PATH = TEST_DATA / ".store"
BUDGET_FILE_PATH = STORE_PATH / "abc123"
