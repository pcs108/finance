from gpg_git_store import StoreFile

from finance import Budget
from finance.presenters import BudgetPresenter

from . import BUDGET_FILE_PATH


def test_budget_presenter(created_store_path):
    budget = Budget(storage=StoreFile(BUDGET_FILE_PATH), id=1, name="hello")
    budget.credits.create_item(name="init", quantity=123, time=1)
    assert BudgetPresenter(budget).text == "  [1] hello 123"
