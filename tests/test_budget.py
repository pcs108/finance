from gpg_git_store import StoreFile

from finance import Budget


def test_budget_balance_adds_up_credits_and_subtracts_debits(created_store_path):
    budget_file = created_store_path / "abc123"
    budget_file.write_text("")
    budget = Budget(id=1, name="fun", storage=StoreFile(path=budget_file))

    budget.credits.create_item(id=1, name="payday", quantity=100, time=1)
    budget.credits.create_item(id=2, name="payday", quantity=50, time=2)
    budget.debits.create_item(id=1, name="pizza", quantity=50, time=3)
    budget.debits.create_item(id=2, name="coffee", quantity=10, time=4)

    assert budget.balance == 90
