import shutil

import pytest

from . import STORE_PATH, TEST_DATA


@pytest.fixture(autouse=True)
def cleanup_files():
    def cleanup():
        if TEST_DATA.exists():
            shutil.rmtree(TEST_DATA)

    cleanup()
    TEST_DATA.mkdir()
    yield
    cleanup()


@pytest.fixture
def store_path():
    return STORE_PATH


@pytest.fixture
def created_store_path(store_path):
    store_path.mkdir()
    return store_path
