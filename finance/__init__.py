from dataclasses import dataclass

from gpg_git_store import RelatedItems, StoreItem

__author__ = "Patrick Schneeweis"
__docformat__ = "markdown en"
__license__ = "GPLv3+"
__title__ = "finance"
__version__ = "0.0.1"


@dataclass(slots=True)
class Credit(StoreItem):
    quantity: int
    time: float


@dataclass(slots=True)
class Debit(StoreItem):
    quantity: int
    time: float


@dataclass(slots=True)
class Budget(StoreItem):
    credits: RelatedItems = RelatedItems(Credit)
    debits: RelatedItems = RelatedItems(Debit)

    completed: bool = False

    @property
    def balance(self) -> int:
        return self.sum_credits - self.sum_debits

    @property
    def sum_credits(self) -> int:
        return sum([c.quantity for c in self.credits.item_list])

    @property
    def sum_debits(self) -> int:
        return sum([d.quantity for d in self.debits.item_list])
