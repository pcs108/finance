import os
import time
from pathlib import Path

import click
from gpg_git_store import ItemListSorter, Store
from gpg_git_store.click import State
from gpg_git_store.click import commands as shared_commands
from gpg_git_store.click import helpers as click_helpers
from gpg_git_store.presenters import ItemListPresenter

from . import Budget
from .presenters import BudgetPresenter

NOW = time.time()

STORE_REQUIRED_ERROR_MESSAGE = """finance has not been set up.
run `fin init` before continuing.
"""

HOME = Path(os.path.expanduser("~"))
DEFAULT_STORE_PATH = HOME / ".finance-store"

STORE_PATH = Path(os.getenv("FINANCE_STORE", DEFAULT_STORE_PATH)).absolute()


@click.group()
@click.pass_context
def cli(ctx: click.Context) -> None:
    try:
        store = Store.from_directory(STORE_PATH, item_class=Budget)
    except FileNotFoundError:
        store = None

    ctx.obj = State(
        store_item_class=Budget,
        store=store,
        store_path=STORE_PATH,
        store_required_error_message=STORE_REQUIRED_ERROR_MESSAGE,
        git_commit_messages_for_commands={
            "add": "Added to budget balance.",
            "create": "Added budget.",
            "decrypt": "Decrypted the store.",
            "encrypt": "Encrypted the store with specified GPG key.",
            "mv": "Renamed specified budget.",
            "spend": "Spent specified budget money.",
            "rm": "Removed specified budget.",
        },
    )


cli.command(shared_commands.init)
cli.command(shared_commands.decrypt)
cli.command(shared_commands.encrypt)
cli.command(shared_commands.git)


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("id", type=int)
@click.option("-n", "--name", type=str, default=f"credit: {NOW}")
@click.option("-q", "--quantity", type=int, prompt=True, required=True)
def add(ctx: click.Context, id: int, name: str, quantity: int) -> None:
    budget = ctx.obj.store.budgets[id]
    budget.credits.create_item(name=name, quantity=quantity, time=NOW)
    presenter = BudgetPresenter(budget)
    click.echo(presenter.text)


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("name")
@click.option("-b", "--balance", type=int, default=0)
def create(ctx: click.Context, name: str, balance: int) -> None:
    budget = ctx.obj.store.create_budget(name=name)
    budget.credits.create_item(name="init", quantity=balance, time=NOW)
    presenter = BudgetPresenter(budget)
    click.echo(presenter.text)


@cli.command()
@click.pass_context
@click_helpers.require_store
def ls(ctx: click.Context) -> None:
    budgets = ctx.obj.store.budget_list
    sorted_budgets = ItemListSorter(items=budgets, attribute="name").results
    output = ItemListPresenter(
        items=sorted_budgets,
        item_presenter_class=BudgetPresenter,
        empty_list_message="No budgets found.",
    ).text
    click.echo(output)


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("id", type=int)
@click.argument("name")
def mv(ctx: click.Context, id: int, name: str) -> None:
    budget = ctx.obj.store.budgets[id]
    budget.update(name=name)
    budget.save()
    presenter = BudgetPresenter(budget)
    click.echo(presenter.text)


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("id", type=int)
@click.option(
    "-f",
    "--force",
    is_flag=True,
    callback=click_helpers.abort_if_false,
    expose_value=False,
    prompt="Delete the budget?",
)
def rm(ctx: click.Context, id: int) -> None:
    budget = ctx.obj.store.budgets[id]
    click.echo(f"Deleting: {budget.name}.")
    budget.delete()


@cli.command()
@click.pass_context
@click_helpers.require_store
@click_helpers.save_git_changes
@click.argument("id", type=int)
@click.option("-n", "--name", type=str, default="debit")
@click.option("-q", "--quantity", type=int, prompt=True, required=True)
def spend(ctx: click.Context, id: int, name: str, quantity: int) -> None:
    budget = ctx.obj.store.budgets[id]
    budget.debits.create_item(name=name, quantity=quantity, time=NOW)
    budget.save()
    presenter = BudgetPresenter(budget)
    click.echo(presenter.text)


def main():
    cli()


if __name__ == "__main__":
    main()
