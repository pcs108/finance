from gpg_git_store.presenters import ItemPresenter


class BudgetPresenter(ItemPresenter):
    @property
    def custom_text(self) -> str:
        return str(self.item.balance)
